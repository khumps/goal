<h1>GOal</h1>
<h3>A golang Rocket League API Wrapper</h3>

[![pipeline status](http://git.khumps.tk/rocket-league/goal/badges/master/pipeline.svg)](http://git.khumps.tk/rocket-league/goal/commits/master)
[![coverage report](http://git.khumps.tk/rocket-league/goal/badges/master/coverage.svg)](http://git.khumps.tk/rocket-league/goal/commits/master)

<h4>Prerequisites</h4>

*  An API Token (If you have access, you can get one at https://api.rocketleague.com/)
*  Basic understanding of go and channels (all queries take a channel to pipe to as a parameter)
  

<h4>Hello World Example (Simple population data return)</h4>

1.  Create an API struct 
```go
exampleAPI := StandardAPI(your_token_here)
```
2.  Create a ServerPopulationRequest 
```go
exampleRequest := exampleAPI.ServerPopulationRequest()
```
3. Get the response
```go
exampleResponse := api.GetServerPopulation(http.DefaultClient, exampleRequest)
```

4Iterate over the different servers in the response and print them
    
```go
for _, response := range exampleResponse.Servers {
    fmt.Printf("\n [%s] Data: \n%+v\n", response.Server, response)
}
``` 

That will yield something that will look like this
```
[PS4] Data:
{Server:PS4 Playlists:[{Name: PlaylistID:-2 NumPlayers:743} {Name: PlaylistID:0 NumPlayers:14385} {Name: PlaylistID:1 NumPlayers:425} {Name: PlaylistID:2 NumPlayers:4161} {Name: PlaylistID:3 NumPlayers:3669} {Name: PlaylistID:4 NumPlayers:328} {Name: PlaylistID:6 NumP
layers:1675} {Name: PlaylistID:7 NumPlayers:1052} {Name: PlaylistID:8 NumPlayers:2387} {Name: PlaylistID:9 NumPlayers:1869} {Name: PlaylistID:10 NumPlayers:849} {Name: PlaylistID:11 NumPlayers:7279} {Name: PlaylistID:12 NumPlayers:185} {Name: PlaylistID:13 NumPlayers:
3816} {Name: PlaylistID:20 NumPlayers:11} {Name: PlaylistID:21 NumPlayers:451} {Name: PlaylistID:22 NumPlayers:43} {Name: PlaylistID:27 NumPlayers:215} {Name: PlaylistID:28 NumPlayers:1024} {Name: PlaylistID:29 NumPlayers:170} {Name: PlaylistID:30 NumPlayers:155} {Nam
e: PlaylistID:32 NumPlayers:840} {Name: PlaylistID:31 NumPlayers:0}]}
2019/08/08 21:12:46
 [Steam] Data:
{Server:Steam Playlists:[{Name: PlaylistID:-2 NumPlayers:585} {Name: PlaylistID:0 NumPlayers:12636} {Name: PlaylistID:1 NumPlayers:254} {Name: PlaylistID:2 NumPlayers:2656} {Name: PlaylistID:3 NumPlayers:2593} {Name: PlaylistID:4 NumPlayers:223} {Name: PlaylistID:6 Nu
mPlayers:1306} {Name: PlaylistID:7 NumPlayers:118} {Name: PlaylistID:8 NumPlayers:241} {Name: PlaylistID:9 NumPlayers:2190} {Name: PlaylistID:10 NumPlayers:744} {Name: PlaylistID:11 NumPlayers:6428} {Name: PlaylistID:12 NumPlayers:292} {Name: PlaylistID:13 NumPlayers:
4225} {Name: PlaylistID:20 NumPlayers:3} {Name: PlaylistID:21 NumPlayers:574} {Name: PlaylistID:22 NumPlayers:30} {Name: PlaylistID:26 NumPlayers:6} {Name: PlaylistID:27 NumPlayers:293} {Name: PlaylistID:28 NumPlayers:598} {Name: PlaylistID:29 NumPlayers:177} {Name: P
laylistID:30 NumPlayers:173} {Name: PlaylistID:32 NumPlayers:367}]}
2019/08/08 21:12:46
 [Switch] Data:
{Server:Switch Playlists:[{Name: PlaylistID:-2 NumPlayers:82} {Name: PlaylistID:0 NumPlayers:779} {Name: PlaylistID:1 NumPlayers:50} {Name: PlaylistID:2 NumPlayers:256} {Name: PlaylistID:3 NumPlayers:361} {Name: PlaylistID:4 NumPlayers:29} {Name: PlaylistID:6 NumPlaye
rs:55} {Name: PlaylistID:7 NumPlayers:69} {Name: PlaylistID:8 NumPlayers:181} {Name: PlaylistID:9 NumPlayers:84} {Name: PlaylistID:10 NumPlayers:63} {Name: PlaylistID:11 NumPlayers:357} {Name: PlaylistID:12 NumPlayers:10} {Name: PlaylistID:13 NumPlayers:191} {Name: Pl
aylistID:21 NumPlayers:15} {Name: PlaylistID:22 NumPlayers:4} {Name: PlaylistID:27 NumPlayers:12} {Name: PlaylistID:28 NumPlayers:82} {Name: PlaylistID:29 NumPlayers:8} {Name: PlaylistID:30 NumPlayers:7} {Name: PlaylistID:32 NumPlayers:101} {Name: PlaylistID:20 NumPla
yers:2}]}
2019/08/08 21:12:46
 [XboxOne] Data:
{Server:XboxOne Playlists:[{Name: PlaylistID:-2 NumPlayers:686} {Name: PlaylistID:0 NumPlayers:17195} {Name: PlaylistID:1 NumPlayers:577} {Name: PlaylistID:2 NumPlayers:5365} {Name: PlaylistID:3 NumPlayers:5054} {Name: PlaylistID:4 NumPlayers:636} {Name: PlaylistID:6 
NumPlayers:2001} {Name: PlaylistID:7 NumPlayers:1094} {Name: PlaylistID:8 NumPlayers:2187} {Name: PlaylistID:9 NumPlayers:2113} {Name: PlaylistID:10 NumPlayers:989} {Name: PlaylistID:11 NumPlayers:7754} {Name: PlaylistID:12 NumPlayers:314} {Name: PlaylistID:13 NumPlay
ers:4622} {Name: PlaylistID:20 NumPlayers:12} {Name: PlaylistID:21 NumPlayers:568} {Name: PlaylistID:22 NumPlayers:61} {Name: PlaylistID:27 NumPlayers:390} {Name: PlaylistID:28 NumPlayers:1531} {Name: PlaylistID:29 NumPlayers:281} {Name: PlaylistID:30 NumPlayers:321} 
{Name: PlaylistID:32 NumPlayers:1202}]}
```

