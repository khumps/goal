package api

import (
	"io/ioutil"
	"log"
	"net/http"
)

// GetResponse gets the http.Response from an http.Request and checks for errors
func GetResponse(client *http.Client, request *http.Request) (response *http.Response, err error) {
	response, err = client.Do(request)
	switch response.StatusCode {
	case 200:
	case 301:
		return
	case 401:
		log.Fatal("Error with API key | ", response.Status)
	default:
		log.Fatal("Call returned ", response.Status)
	}
	return
}

func getRawJSON(api API, client *http.Client, request *http.Request) ([]byte, error) {
	httpResp, err := GetResponse(client, request)
	defer httpResp.Body.Close()
	raw, err := ioutil.ReadAll(httpResp.Body)
	return raw, err
}
