package api

import "strings"

// RequestType All availible request types to the API
type RequestType string

// RequestType All RequestTypes
const (
	RequestPopulation        RequestType = "population"
	RequestSkills            RequestType = "player-skills"
	RequestPlayerTitle       RequestType = "player-titles"
	RequestRegion            RequestType = "region"
	RequestSkillsLeaderboard RequestType = "skills-leaderboard"
	RequestStatsLeaderboard  RequestType = "stats-leaderboard"
)

// Servers All availible servers on the API
type Servers string

// Servers All Servers
const (
	PS4   Servers = "ps4"
	Steam Servers = "steam"
	Xbox  Servers = "xboxone"
)

func (s Servers) String() string {
	return string(s)
}

// StatType Represents any stat that can be requested by the endpoints
type StatType string

// StatType All types of stats
const (
	StatAssists StatType = "assists"
	StatGoals   StatType = "goals"
	StatMVP     StatType = "mvps"
	StatSaves   StatType = "saves"
	StatShots   StatType = "shots"
	StatWins    StatType = "wins"
	StatInvalid StatType = "invalid"
)

func getStatType(t string) StatType {
	switch strings.ToLower(t) {
	case string(StatAssists):
		return StatAssists
	case string(StatGoals):
		return StatGoals
	case string(StatMVP):
		return StatMVP
	case string(StatSaves):
		return StatSaves
	case string(StatShots):
		return StatShots
	case string(StatWins):
		return StatWins
	default:
		return StatInvalid
	}

}
