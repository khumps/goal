package api

import (
	"fmt"
	"os"
	"testing"
)

func testAPI() API {
	return StandardAPI(os.Getenv("RL_TOKEN"))
}
func TestAPI(t *testing.T) {
	api := testAPI()
	t.Log(api)
}
func TestServerPopulationRequest(t *testing.T) {
	api := testAPI()
	serverPopulationRequest := api.NewServerPopulationRequest()
	t.Log(serverPopulationRequest)
}

func TestGetServerPopulationResponse(t *testing.T) {
	api := testAPI()
	request := api.NewServerPopulationRequest()
	response := api.GetServerPopulation(client, request)
	if response.err != nil {
		t.Log("Error ", response.err)
		t.Fail()
	} else {
		for _, response := range response.Servers {
			t.Logf("\n [%s] Data: \n%+v\n", response.Server, response)
		}
	}
}

func TestPlayerSkillsRequest(t *testing.T) {
	api := testAPI()
	playerSkillsRequest := api.NewPlayerSkillsRequest(PS4, "khumps")
	t.Logf("\nplayerSkillsRequest: \n %+v\n", playerSkillsRequest)
}

func TestMultiPlayerSkillsRequest(t *testing.T) {
	api := testAPI()
	playerSkillsRequest := api.NewMultiPlayerSkillsRequest(PS4, []string{"khumps"})
	t.Logf("\nplayerSkillsRequest: \n %+v\n", playerSkillsRequest)
}

func TestGetPlayerSkillsResponse(t *testing.T) {
	api := testAPI()
	request := api.NewPlayerSkillsRequest(Steam, "76561198082498505")
	response := api.GetPlayerSkills(client, request)
	if len(response) == 0 {
		t.Fail()
	} else {
		t.Logf("\nResponse Struct: \n%+v\n", response)
	}
}

func TestGetMultiPlayerSkillsResponse(t *testing.T) {
	api := testAPI()
	request := api.NewMultiPlayerSkillsRequest(Steam, []string{"76561198082498505"})
	fmt.Printf("Request: %+v\n", request.GetRequest().Header)
	response := api.GetPlayerSkills(client, request)
	if len(response) == 0 {
		t.Fail()
	} else {
		t.Logf("\nResponse Struct: \n%+v\n", response)
	}
}

func TestPlayerTitlesRequest(t *testing.T) {
	api := testAPI()
	playerTitlesRequest := api.NewPlayerTitlesRequest(Steam, "76561198082498505")
	t.Logf("\n playerTitlesRequest: \n %+v\n", playerTitlesRequest)
}

func TestGetPlayerTitlesResponse(t *testing.T) {
	api := testAPI()
	playerTitlesRequest := api.NewPlayerTitlesRequest(Steam, "76561198082498505")
	response := api.GetPlayerTitles(client, playerTitlesRequest)
	if response.err != nil {
		t.Log("Error ", response.err)
		t.Fail()
	}
	t.Logf("\nResponse Struct: \n%+v\n", response)
}

func TestRegionsRequest(t *testing.T) {
	api := testAPI()
	regionsRequest := api.NewRegionsRequest()
	t.Logf("\n regionsRequest: \n %+v\n", regionsRequest)
}

func TestGetRegions(t *testing.T) {
	api := testAPI()
	regionsRequest := api.NewRegionsRequest()
	response := api.GetRegions(client, regionsRequest)
	if response.err != nil {
		t.Log("Error ", response.err)
		t.Fail()
	}
	t.Logf("\n Response Struct: \n %+v\n", response)
}

func TestSkillsLeaderboardRequest(t *testing.T) {
	api := testAPI()
	skillsLeaderboardRequest := api.NewSkillsLeaderboardRequest(Steam, "10")
	t.Logf("\n regionsRequest: \n %+v\n", skillsLeaderboardRequest)
}

func TestGetSkillsLeaderboard(t *testing.T) {
	api := testAPI()
	skillsLeaderboardRequest := api.NewSkillsLeaderboardRequest(Steam, "10")
	response := api.GetSkillsLeaderboard(client, skillsLeaderboardRequest)
	if response.err != nil {
		t.Log("Error ", response.err)
		t.Fail()
	}
	t.Logf("\n Response Struct: \n %+v\n", response)
}

func TestGetStatsLeaderboard(t *testing.T) {
	api := testAPI()
	statsLeaderboardRequest := api.NewMultiStatsLeaderboardRequest(Steam)
	//statsLeaderboardRequest := NewStatsLeaderboardRequest(Steam, StatGoals)
	response := api.GetStatsLeaderboard(client, statsLeaderboardRequest)
	if response.err != nil {
		t.Log("Error ", response.err)
		t.Fail()
	}
	t.Logf("\n Response Struct: \n %+v\n", response)
}
