package api

// Contains ways to generate all of the different response types the API can return
import (
	"encoding/json"
	"errors"
	"net/http"
)

// GetServerPopulation Returns a new []ServerPopulationResponse with data for every server defined in ServerPopulationResponses
func (api API) GetServerPopulation(client *http.Client, request APIRequest) ServerPopulationResponseSet {
	api.AuthorizeRequest(request)
	servers := NewServerPopulationResponseSet()
	if request.requestType != RequestPopulation {
		servers.err = errors.New("You passed a non-PopulationResponse request to GetServerPopulationResponse()")
		return servers
	}
	raw, err := getRawJSON(api, client, request.GetRequest())
	if err != nil { // Error reading raw data
		servers.err = err
		return servers
	}
	var jsonData map[string]*json.RawMessage
	json.Unmarshal(raw, &jsonData)   // Unmarshal list of servers into raw json objects
	for i := range servers.Servers { // Unmarshal each server into a server object
		if err = json.Unmarshal(*jsonData[servers.Servers[i].Server], &servers.Servers[i].Playlists); err != nil {
			servers.err = err
		}
	}
	return servers
}

// GetPlayerSkills Returns a struct containing all data from a PlayerSkills GET request
func (api API) GetPlayerSkills(client *http.Client, request APIRequest) []PlayerSkillsResponse {
	api.AuthorizeRequest(request)
	var skillsArray []PlayerSkillsResponse
	if request.GetRequestType() != RequestSkills {
		skillsArray[0].err = errors.New("You passed a non-PlayerSkills request to GetPlayerSkillsResponse()")
		return skillsArray
	}
	raw, err := getRawJSON(api, client, request.GetRequest())
	if err != nil { // Error reading raw data
		skillsArray[0].err = err
		return skillsArray
	}
	if err = json.Unmarshal(raw, &skillsArray); err != nil { // Unmarshal raw JSON bytes onto playerSkills struct
		skillsArray[0].err = err
	}
	return skillsArray
}

// GetPlayerTitles Returns a PlayerTitlesResponse struct containing all the titles returned by this request for a player
func (api API) GetPlayerTitles(client *http.Client, request APIRequest) PlayerTitlesResponse {
	api.AuthorizeRequest(request)
	playerTitles := PlayerTitlesResponse{}
	if request.GetRequestType() != RequestPlayerTitle {
		playerTitles.err = errors.New("You passed a non-PlayerTitles request to GetPlayerTitlesResponse()")
		return playerTitles
	}

	raw, err := getRawJSON(api, client, request.GetRequest())
	if err != nil { // Error reading raw data
		playerTitles.err = err
		return playerTitles
	}

	if err = json.Unmarshal(raw, &playerTitles); err != nil { // Unmarshal raw JSON bytes onto playerSkills struct
		playerTitles.err = err
	}
	return playerTitles
}

// GetRegions Returns a RegionsResponse set containing all of the region data
func (api API) GetRegions(client *http.Client, request APIRequest) RegionResponseSet {
	api.AuthorizeRequest(request)
	regionResponseSet := RegionResponseSet{}
	if request.GetRequestType() != RequestRegion {
		regionResponseSet.err = errors.New("You passed a non-RegionRequest request to GetRegions()")
		return regionResponseSet
	}
	raw, err := getRawJSON(api, client, request.GetRequest())
	if err != nil {
		regionResponseSet.err = err
		return regionResponseSet
	}
	if err = json.Unmarshal(raw, &regionResponseSet.Regions); err != nil {
		regionResponseSet.err = err
	}
	return regionResponseSet
}

// GetSkillsLeaderboard Returns a SkillsLeaderboardResponseSet
func (api API) GetSkillsLeaderboard(client *http.Client, request APIRequest) SkillsLeaderboardResponseSet {
	api.AuthorizeRequest(request)
	skillsResponseSet := SkillsLeaderboardResponseSet{}
	if request.GetRequestType() != RequestSkillsLeaderboard {
		skillsResponseSet.err = errors.New("You passed a non-SkillsLeaderboardRequest request to GetSkillsLeaderboard()")
		return skillsResponseSet
	}
	raw, err := getRawJSON(api, client, request.GetRequest())
	if err != nil {
		skillsResponseSet.err = err
		return skillsResponseSet
	}
	if err = json.Unmarshal(raw, &skillsResponseSet.Leaders); err != nil {
		skillsResponseSet.err = err
	}
	return skillsResponseSet
}

// GetStatsLeaderboard Returns a StatsLeaderboardResponseSet containing all stat data for the request. Can handle any type of StatsLeaderboardRequest
func (api API) GetStatsLeaderboard(client *http.Client, request APIRequest) StatsLeaderboardResponseSet {
	api.AuthorizeRequest(request)
	statsResponseSet := StatsLeaderboardResponseSet{}
	if request.GetRequestType() != RequestStatsLeaderboard {
		statsResponseSet.err = errors.New("You passed a non-StatsLeaderboardRequest request to GetStatsLeaderboard()")
		return statsResponseSet
	}
	raw, err := getRawJSON(api, client, request.GetRequest())
	if err != nil {
		statsResponseSet.err = err
		return statsResponseSet
	}
	var a []*json.RawMessage
	var m map[string]interface{}
	if err = json.Unmarshal(raw, &a); err != nil { // Convert raw JSON into an array of JSON objects (each object represents an individual stat leaderboard)
		statsResponseSet.err = err
		return statsResponseSet
	}
	for i := range a { // Iterate over each stat leaderboard returned, append it to the list of leaderboards (Stats)
		if err = json.Unmarshal(*a[i], &m); err != nil {
			statsResponseSet.err = err
			return statsResponseSet
		}
		statsResponseSet.Stats = append(statsResponseSet.Stats, statsLeaderboardResponse{StatType: getStatType(m["stat_type"].(string))})
		for j := range m["stats"].([]interface{}) { // Iterate over each player in the leaderboard and parse their data, append to the list of entries(Entries)
			rowString := m["stats"].([]interface{})[j].(map[string]interface{})
			statsResponseSet.Stats[i].Entries = append(statsResponseSet.Stats[i].Entries, statLeaderboardEntry{Count: rowString[string(statsResponseSet.Stats[i].StatType)].(float64), Username: rowString["user_name"].(string), UserID: rowString["user_id"].(string)})
		}
	}
	return statsResponseSet
}
