package api

import (
	"fmt"
	"os"
)

// API contains your token to interface with the API
type API struct {
	Token        string
	BaseEndpoint string
}

// StandardAPI returns a new API with your token and the standard endpoint
func StandardAPI(token string) API {
	return API{token, "https://api.rocketleague.com/"}
}

// String returns string representation of the API object
func (api API) String() string {
	if os.Getenv("DEV_ENVIRONMENT") == "true" {
		return fmt.Sprintf("[API] Base Endpoint: %s | Token: %s", api.BaseEndpoint, "REDACTED")
	}
	return fmt.Sprintf("[API] Base Endpoint: %s | Token: %s", api.BaseEndpoint, api.Token)
}

// AuthorizeRequest takes in a request and sets the "Authorization" header
func (api API) AuthorizeRequest(r APIRequest) APIRequest {
	r.request.Header.Set("Authorization", "Token "+api.Token)
	return r
}
