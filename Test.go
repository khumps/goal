package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

//Response data for the api/v1/population endpoint

var client = &http.Client{}

func main() {
	API := API{Token: os.Getenv("RL_TOKEN"), BaseEndpoint: "https://api.rocketleague.com/"}
	populationRequest := API.NewServerPopulationRequest()
	resp, respErr := client.Do(populationRequest.GetRequest())
	if respErr != nil {
		log.Fatal(respErr)
	}
	defer resp.Body.Close()
	raw, _ := ioutil.ReadAll(resp.Body)
	var jsonData map[string]*json.RawMessage
	json.Unmarshal(raw, &jsonData)
}

func testUnmarshallAndPlaylists(raw []byte) {
	var jsonData map[string]*json.RawMessage
	json.Unmarshal(raw, &jsonData)
	fmt.Println("------ Raw JSON ------")
	fmt.Println(string(raw))
	fmt.Println("------ After Inital Unmarshal ------")
	fmt.Println(jsonData)
	var xboxPlaylists []Playlist
	fmt.Println("Xbox raw | " + string(*jsonData["XboxOne"]))
	err := json.Unmarshal(*jsonData["XboxOne"], &xboxPlaylists)
	if err != nil {
		log.Fatal("Error: " + err.Error())
	}
	//getJSON(request, &popResponse)
	fmt.Println("------ ACTUAL ------")
	fmt.Println(xboxPlaylists)
}

func testUnmarshallSkills(raw []byte) {
	fmt.Println(string(raw))
}
