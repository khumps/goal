package api

// Contains all of the different structs used fore responses including the ones used to unmarshal JSON Objects

// NewServerPopulationResponseSet returns a new ServerPopulationResponseSet
func NewServerPopulationResponseSet() ServerPopulationResponseSet {
	return ServerPopulationResponseSet{Servers: []ServerPopulationResponse{
		ServerPopulationResponse{Server: "Epic"},
		ServerPopulationResponse{Server: "PS4"},
		ServerPopulationResponse{Server: "Steam"},
		ServerPopulationResponse{Server: "Switch"},
		ServerPopulationResponse{Server: "XboxOne"}}}
}

// ServerPopulationResponseSet contains all server responses from the api/v1/population endpoint
type ServerPopulationResponseSet struct {
	err     error
	Servers []ServerPopulationResponse
}

// ServerPopulationResponse contains a server from the api/v1/population endpoint
type ServerPopulationResponse struct {
	Server    string
	Playlists []Playlist
}

// Playlist Single playlist response from a server
type Playlist struct {
	Name       string
	PlaylistID int
	NumPlayers int
}

// PlayerSkillsResponse contains all of the data returned from a PlayerSkillsRequest
type PlayerSkillsResponse struct {
	err           error
	UserName      string          `json:"user_name"`
	UserID        string          `json:"user_id"`
	PlayerSkills  []SkillResponse `json:"player_skills"`
	SeasonRewards struct {
		Wins  int
		Level int
	} `json:"season_rewards"`
}

// SkillResponse represents a single part of the PlayerSkillsResponse
type SkillResponse struct {
	Division      int
	Playlist      int
	MU            float32
	WinStreak     int
	Tier          int
	Skill         int
	Sigma         float32
	MatchesPlayed int
	TierMax       int
}

// PlayerTitlesResponse contains all of a players titles returned from a PlayerTitlesRequest
type PlayerTitlesResponse struct {
	err    error
	Titles []string
}

// RegionResponseSet contains all data returned from a RegionsRequest
type RegionResponseSet struct {
	err     error
	Regions []RegionResponse
}

// RegionResponse contains all the data from one region from a RegionsRequest
type RegionResponse struct {
	Region    string
	Platforms string
}

// SkillsLeaderboardResponseSet contains any errors thrown by a SkillsLeaderboardRequest as well as a list of all the leaders the response returned
type SkillsLeaderboardResponseSet struct {
	err      error
	Platform string
	Playlist string
	Leaders  []SkillsLeaderboardResponse
}

// SkillsLeaderboardResponse contains all data from a single entry in a SkillsLeaderboardRequest
type SkillsLeaderboardResponse struct {
	UserName string `json:"user_name"`
	Skill    int
	Tier     int
}

// StatsLeaderboardResponseSet contains all data from a GetStatsLeaderboard call
type StatsLeaderboardResponseSet struct {
	err    error
	Server Servers
	Stats  []statsLeaderboardResponse
}

type statsLeaderboardResponse struct {
	StatType StatType
	Entries  []statLeaderboardEntry
}

type statLeaderboardEntry struct {
	Count    float64
	Username string
	UserID   string
}
