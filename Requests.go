package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type APIRequest struct {
	requestType RequestType
	request     *http.Request
}

func (req APIRequest) GetRequestType() RequestType {
	return req.requestType
}

func (req APIRequest) GetRequest() *http.Request {
	return req.request
}

// NewServerPopulationRequest Returns a request of the server population
func (api API) NewServerPopulationRequest() APIRequest {
	req, _ := http.NewRequest(http.MethodGet, api.BaseEndpoint+"/api/v1/population", nil)
	return APIRequest{
		requestType: RequestPopulation,
		request:     req,
	}
}

// NewPlayerSkillsRequest Returns a request for all of a players skill from the given platform
func (api API) NewPlayerSkillsRequest(platform Servers, playerID string) APIRequest {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(api.BaseEndpoint+"/api/v1/%s/playerskills/%s", platform, playerID), nil)
	return APIRequest{
		requestType: RequestSkills,
		request:     req,
	}
}

func (api API) NewMultiPlayerSkillsRequest(platform Servers, playerIDs []string) APIRequest {
	body := map[string]interface{}{}
	body["player_ids"] = playerIDs
	idBytes, _ := json.Marshal(body)
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(api.BaseEndpoint+"/api/v1/%s/playerskills", platform), bytes.NewReader(idBytes))
	return APIRequest{
		requestType: RequestSkills,
		request:     req,
	}
}

// NewPlayerTitlesRequest Returns a request for all of a players titles on the given platform
func (api API) NewPlayerTitlesRequest(platform Servers, playerID string) APIRequest {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(api.BaseEndpoint+"/api/v1/%s/playertitles/%s", platform, playerID), nil)
	return APIRequest{
		requestType: RequestPlayerTitle,
		request:     req,
	}
}

// NewRegionsRequest Returns a request for all of the regions
func (api API) NewRegionsRequest() APIRequest {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(api.BaseEndpoint+"/api/v1/regions"), nil)
	return APIRequest{
		requestType: RequestRegion,
		request:     req,
	}
}

// NewSkillsLeaderboardRequest Generates a GetRequest of type SkillsLeaderboartdRequest for the specific platform and playlist
func (api API) NewSkillsLeaderboardRequest(platform Servers, playlist string) APIRequest {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(api.BaseEndpoint+"/api/v1/%s/leaderboard/skills/%s", platform, playlist), nil)
	return APIRequest{
		requestType: RequestSkillsLeaderboard,
		request:     req,
	}
}

// NewStatsLeaderboardRequest Generates a GetRequest of type StatsLeaderboardRequest for the given platform and statType
func (api API) NewStatsLeaderboardRequest(server Servers, statType StatType) APIRequest {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(api.BaseEndpoint+"/api/v1/%s/leaderboard/stats/%s", server, statType), nil)
	return APIRequest{
		requestType: RequestStatsLeaderboard,
		request:     req,
	}
}

// NewMultiStatsLeaderboardRequest Generates a GetRequest of type StatsLeaderboardRequest for the given platform
func (api API) NewMultiStatsLeaderboardRequest(server Servers) APIRequest {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(api.BaseEndpoint+"/api/v1/%s/leaderboard/stats", server), nil)
	return APIRequest{
		requestType: RequestStatsLeaderboard,
		request:     req,
	}
}
